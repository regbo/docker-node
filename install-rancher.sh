#setup new root password
echo "Enter a new root password, or blank for no change"
read ROOT_PWD
if [ -z "$ROOT_PWD" ] ; then
    echo 'root password will not be changed'
else
    echo "root:$ROOT_PWD" | chpasswd
fi


#setup hostname
echo "Enter a new hostname, or blank for no change"
read HOSTNAME_UPDATE
if [ -z "$HOSTNAME_UPDATE" ] ; then
    echo 'hostname will not be changed'
else
    hostnamectl set-hostname $HOSTNAME_UPDATE
fi

#setup hostname
echo "Enter a docker root for storage (EX OVH: /home/var/lib/docker), or blank for no change"
read DOCKER_ROOT
if [ -z "$DOCKER_ROOT" ] ; then
    echo 'docker root will not be changed'
    DOCKER_ROOT=""
else
    DOCKER_ROOT=", \"data-root\": \"$DOCKER_ROOT\""
fi

#setup rancher

echo "Enter your rancher setup command"
read DOCKER_CMD
if [ -z "$DOCKER_CMD" ] ; then
    echo 'the command from rancher must be present'
    exit 1
fi

HOST_CHECK=`getent hosts yahoo.com | awk '{ print $1 }'`
if [ -z "$HOST_CHECK" ]; then
    echo "dns resolution failed, adding google dns servers"
    sudo sed -i 's/nameservers: {}/nameservers:\n             addresses: [8.8.8.8, 8.8.4.4]/g' /etc/netplan/*.yaml
    sudo netplan apply
fi
apt --fix-broken -y install
sysctl -w vm.max_map_count=262144
apt-get purge -y docker
set -e
apt-get -y update
apt-get -y upgrade
if [ $(dpkg-query -W -f='${Status}' unattended-upgrades 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  apt-get install -y unattended-upgrades;
fi
apt-get install -y curl nano glances docker.io
echo "{\"dns\": [\"8.8.8.8\", \"8.8.4.4\"], \"default-ulimit\": 1048576$DOCKER_ROOT}" > /etc/docker/daemon.json
service docker restart
systemctl enable docker
eval $DOCKER_CMD
printf "APT::Periodic::Update-Package-Lists \"1\";\r\nAPT::Periodic::Download-Upgradeable-Packages \"1\";\r\nAPT::Periodic::AutocleanInterval \"7\";\r\nAPT::Periodic::Unattended-Upgrade \"1\";" > /etc/apt/apt.conf.d/20auto-upgrades
service unattended-upgrades restart

